# An example of using a class variable which counts instances
import random

class ClassVar:
    """ Class variable example """
    # keeps a count of instances
    count = 0
    def __init__(self):
        ClassVar.count += 1
        self.number = random.randint(1,100)
        print 'my number is', self.number 

    def say_hi(self):
        print 'Hi'

    #@classmethod
    def take_one(self):
        self.number -= 1
        
a = ClassVar()
b = ClassVar()
c = ClassVar()

a.take_one()

print a.number
print b.number
print c.number
