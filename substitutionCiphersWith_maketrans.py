# coding=UTF-8
# http://www.python.org/peps/pep-0263.html
"""
This module implements a simple substitute cipher
using the maketrans() and translate functions of the
string module
"""


# imports
import sys
import string
# constants
# exception classes
# interface functions
# classes
# internal functions & classes

class Simplecipher(object):
	"""Simplecipher is a implementation of a simple substution cipher"""
	def __init__(self):
		self._from = self.make_from()
		self._to = self.make_to()
		self.encode_table = self.encoding_table()
		self.decode_table = self.decoding_table()
	
	def make_from(self):
		""" Create the full character set that our message uses """
		# 'abcdefghijklmnopqrstuvwxyz0123456789'
		return string.ascii_lowercase + string.digits

	def make_to(self):
		""" Return a string of 36 character used to substitute with """
		# 'chr' converts ascii code to an ascii character
		result = "".join([chr(i+32) for i in range(1,37)]) 
		return result

	def encoding_table(self):
		if len(self._from) != len(self._to):
			raise "The inputs to maketans() are not of the same length"
		else:
			# from > to
			return string.maketrans(self._from, self._to)

	def decoding_table(self):
		if len(self._from) != len(self._to):
			raise "The input to maketans() are not of the same length"
		else:
			# to > from
			return string.maketrans(self._to, self._from)

	def make_secret(self,message=None):
		""" cipher an input string """
		result = string.translate(message, self.encode_table)
		return result

	def find_message(self,secret_message=None):
		""" decode an inout string """
		result = string.translate(secret_message, self.decode_table)
		return result
		

def main():
	# create a new Simplecipher object
	simp_ciph = Simplecipher()
	# the message we want to cipher
	message = 'the quick brown fox jumps over the lazy dog'
	# cipher the message
	secret = simp_ciph.make_secret(message)

	print message
	print secret
	
	# decode the secret message
	print simp_ciph.find_message(secret)
	
if __name__ == '__main__':
    status = main()
    sys.exit(status)