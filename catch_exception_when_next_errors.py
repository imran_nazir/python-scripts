# Author: Imran Nazir
# Date: 14/9/2014
# Purpose: Create an iterator and reurn values as long as you can
# break on error

a = [1,2,3,4]
b = iter(a)

while True:
    try:
        print b.next()
    except:
        print 'end of the road'
        break
