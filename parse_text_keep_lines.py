#   Date: 5,/Nov/2015
#   Purpose: Steps though the output file
#   and display the values. Usr can save the url
#   of any bad data and look at those links later

import os
import pprint

os.chdir('/home/timebandit/Documents/iedfautomation/outputs/3')


with open('out.txt','r') as f:
    lines = f.readlines()

# the output of urls that need checking
bad_urls = []
# url of current items being looked at
current_url = ""

output = """
********************

    %s

********************
"""

for line in lines:
    if "Url : " in line:
        # extract the url information
        current_url = line.partition(" : ")[2]
    if "AuthorSite : " in line:
        print output % line.partition(" : ")[2]
        answer = raw_input("If this is poorly formatted press 'k' otherwise hit any key")
        print answer
        if answer == 'k':
            bad_urls.append(current_url)
        else:
            continue

f = open("cleaned-output.txt", "w")
f.write(repr(bad_urls))
f.close()
