from time import sleep
from math import sin, cos, radians

top = """                              
                     ,o8888boo.
               b   d88888888888b
              d88oo88888888888888o.
     /\      8888888888P" Y88888888o,b
  _--/ \     88888888P".''.Y88P" Y8888
 /__/   "\   Y888888P '.-.  Y" _. Y88P
<______   "\  "8888P  /  |    '_ '$88
       \,    \  888b  Bo |    / \ a8Z
         \     \ 8|<  B8/     Bo||P"
          '.   /'--\     ._   P8/|
            \ /     '\_  \     _/ 
             \        /'-----.'
              \      |  /  |\ \ 
               '-./  | / \./|  \  
                  |  \/   |/    |
              ,---|       /\__./
            _/    '-.___./ ;   \ 
           /             \  \   """

bottom = """
      --._/               \  \   \_ 
       \                   \._\    "--.
        \                    _''-._<'' 
         \                  /" 
          '-.            .-"   
             "----._____-|
                |    |   |
               |     |   |
               |     |   |
               |      I  |
               |      |   T 
               |      |   | 
               |      |   | 
               |      |    \
               |      "|    |
               |       |    Z
                "-.__.-P__.--
                \88ooo8|PwwPP
                  """"'        ch1x0r """
class Curve:
	def curve(self):
		for i in range(1,361):
			circ1 = 10 * (1 + sin(radians(i*10)))
			circ2 = 10 * (1 + cos(radians(i*10)))
			print (top).center(int(circ1)*100)
			print (bottom).center(int(circ2)*100)
			sleep(0.09)

a = Curve()
a.curve()
#print bottom	
# 080 084 0870
# les.harvey ext 22839