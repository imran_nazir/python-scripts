# Author:   Imran Nazir
# Date:     21/08/14
# Purpose:  Extract a table from a website. Return as an array
# 

from bs4 import BeautifulSoup
import urllib
import urllib2
import time
URL = 'http://pymotw.com/2/py-modindex.html'

class Extractor(object):
    
    def __init__(self, url=URL):
        """ init the class """
        self.url=url

    def __make_http_header(self):
        return {
                'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Language' : 'en-us,en;q=0.5',
                'User-Agent' : 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:28.0) Gecko/20100101 Firefox/28.0'
        }
        
    def read_url(self, content= False):
        """ Fetches raw html contents of url/rss """
        try:
            req = urllib2.Request(self.url, None, self.__make_http_header())
            opener = urllib2.build_opener(urllib2.HTTPCookieProcessor())
            urllib2.install_opener(opener)
            f = opener.open(req, timeout=10)
            if content:
                return f.geturl(), f.read()
            else:
                return f.geturl()
        except Exception, e:
            Logger.error("Unable to retrieve or process URL " + url + ': ' + str(e) + '. Skipping ...')
            return None

    def extracttable(self, content):
        """ extract the table, 'None' if not found """
        soup = BeautifulSoup(content)
        table = soup.find('table')
        return table

    def tabletolist(self, table):
        """ convert the html table to an list see http://bit.ly/1q2Fzz4"""
        result = []
        allrows = table.findAll('tr')
        for row in allrows:
            result.append([])
            allcols = row.findAll('td')
            for col in allcols:
                thestrings = [unicode(s) for s in col.findAll(text=True)]
                thetext = ''.join(thestrings)
                result[-1].append(thetext)
        return result



e = Extractor(URL)
content = e.read_url(True)
table = e.extracttable(content[1])
l = e.tabletolist(table)
for item in l:
    if item:
        print item[1].strip(), "\t", item[0]
