* This repo contains source files I created to solve particular problems using Python. 
* The code is available to you under the GNU licence. 
* If you use any of this code then please attribute it to me.