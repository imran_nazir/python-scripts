# Author:   Imran Nazir
# Date:     21/08/14
# Purpose:  Extract the flight information from Luton Airport
# 

from bs4 import BeautifulSoup
import urllib
import urllib2
import time
import logging
URL = 'http://m.london-luton.co.uk/1259/777/FlightFeedResults?s=N8L1BE9383MJV&flight=FR342&airport=&type=arrivals'

class Extractor(object):
    
    def __init__(self, url=URL):
        """ init the class """
        self.url=url

    def __make_http_header(self):
        return {
                'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Language' : 'en-us,en;q=0.5',
                'User-Agent' : 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:28.0) Gecko/20100101 Firefox/28.0'
        }
        
    def read_url(self, content= False):
        """ Fetches raw html contents of url/rss """
        try:
            req = urllib2.Request(self.url, None, self.__make_http_header())
            opener = urllib2.build_opener(urllib2.HTTPCookieProcessor())
            urllib2.install_opener(opener)
            f = opener.open(req, timeout=10)
            if content:
                return f.geturl(), f.read()
            else:
                return f.geturl()
        except Exception, e:
            logging.error("Unable to retrieve or process URL " + URL + ': ' + str(e) + '. Skipping ...')
            return None

    def get_flight_status(self, content):
        """ extract the table, 'None' if not found """
        soup = BeautifulSoup(content)
        status = soup.find(class_="sFlightResultStatus")
        return status.text

    def tabletolist(self, table):
        """ convert the html table to an list see http://bit.ly/1q2Fzz4"""
        result = []
        allrows = table.findAll('tr')
        for row in allrows:
            result.append([])
            allcols = row.findAll('td')
            for col in allcols:
                thestrings = [unicode(s) for s in col.findAll(text=True)]
                thetext = ''.join(thestrings)
                result[-1].append(thetext)
        return result



e = Extractor(URL)
content = e.read_url(True)
status = e.get_flight_status(content[1])
print status
