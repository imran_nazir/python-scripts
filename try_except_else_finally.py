try:
	print "I'll try not to raise an exception"
	print 1/0
except Exception, e:
	print e
else:
	print "No exceptions were raised"
finally:
	print "got to do this at the end"