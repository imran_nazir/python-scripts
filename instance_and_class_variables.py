class Food:
    kind = 'fruit'

    def __init__(self, name):
        self.name = name

a = Food('tomato')
b = Food('cabbage')

print 'a ->', a.kind
print 'b ->', b.kind
print 'change a\'s kinde'
a.kind = 'veg'
print 'a ->', a.kind
print 'b ->',b.kind
print 'change kind in the class'
Food.kind = 'meat'
print 'a ->', a.kind
print 'b ->', b.kind
