def imcalled():
    print 'outside call'

class Test(object):
    """ calling a global method with same name """
    def imcalled(self):
        print 'inside call'
        # call the external method
        return imcalled()
        # call this method
        return self.imcalled()

a = Test()
a.imcalled()
