# Author:   Imran Nazir
# Date: 16/09/2014
# Purpose:  To create a csv file for the standard Python types

import types

def list_types():
    # list the main python types
    result = [t for t in vars(types).values() if isinstance(t, type)]
    return result

def build_method_hash():
    # create a hash {type:[public type methods, x, x],...}
    types = list_types()
    result = {}
    for typ in types:
        methods = dir(typ)
        for method in methods:
            if typ not in result:
                result[type] = [method]
            else:
                result[type].append(method)
    return result

print build_method_hash()
